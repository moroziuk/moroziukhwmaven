package HomeWork4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountFamilyTest {
    @Test
    public void testCountFamily() {
        Human a = new Human();
        Human b = new Human();
        Human c = new Human();

        Family f = new Family(a, b);
        f.addChild(c);

        int count = f.countFamily();
        assertEquals(3,count);
    }
}
