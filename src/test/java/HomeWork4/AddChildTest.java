package HomeWork4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AddChildTest {
    @Test
    public void testAddChild() {
        Human a = new Human();
        Human b = new Human();
        Human c = new Human();
        Family f = new Family(a, b);

        int fl = f.getChildren().size();
        f.addChild(c);

        int sl = f.getChildren().size();

        assertEquals(1,sl-fl);
        assertNotEquals(0, sl-fl);
    }

    @Test
    //
    public void testAddChild2() {
        Human a = new Human();
        Human b = new Human();
        Human c = new Human();
        Human d = new Human();

        Family f = new Family(a, b);

        f.addChild(c);
        f.addChild(d);

        int sl = f.getChildren().size()-1;

        assertEquals(c,f.getChildren().get(sl));
    }

}
