package HomeWork4;

import org.junit.Test;

import static org.junit.Assert.*;

public class DeleteChildTest {
    @Test
    public void testDeleteChild() {
        Human a = new Human();
        Human b = new Human();
        Human c = new Human();
        Human d = new Human();
        Human e = new Human();
        Human g = new Human();

        Family f = new Family(a, b);
        f.addChild(c);
        f.addChild(d);
        f.addChild(e);
        f.addChild(g);
        f.deleteChild(d);

        int count = f.getChildren().size();
        assertEquals(3,count);
    }
    @Test
    public void testDeleteChild2() {
        Human a = new Human();
        Human b = new Human();
        Human c = new Human();
        Human d = new Human();
        Human e = new Human();
        Human g = new Human();

        Family f = new Family(a, b);
        f.addChild(c);
        f.addChild(d);
        f.addChild(e);
        f.addChild(g);
        f.deleteChild(3);

        assertTrue(f.deleteChild(2));

        assertFalse(f.deleteChild(5));

        int count = f.getChildren().size();
        assertEquals(2,count);
    }
}
