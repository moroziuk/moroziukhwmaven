package HomeWork4;

import HomeWork4.pets.Fish;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class ToStringTest {
    @Test
    public void testToString() {
        Fish pet = new Fish
                ("Coco", 12, 55,"running","playing");

        String expected = "Fish:{nickname=:Coco, age:12, trickLevel=:55,habits=:[running, playing]}\n";
        assertEquals(expected, pet.toString());
        assertNotEquals("Basbfasf", pet.toString());
    }
    @Test
    public void testToString2() {
        Human human = new Human
                ("a", "b", LocalDate.of(2000,1,12));

        String expected = "Human{name=:a, surname:b, birthdate=:12/01/2000, iq=:0, schedule=null}\n";
        assertEquals(expected, human.toString());
        assertNotEquals("Basbfasf", human.toString());
    }
    @Test
    public void testToString3() {
        Human a = new Human
                ("a", "z", LocalDate.of(2000,1,12));
        Human b = new Human
                ("b", "y", LocalDate.of(2000,1,12));
        Family family = new Family(a,b);
        String expected =
                "Human{name=:a, surname:z, birthdate=:12/01/2000, iq=:0, schedule=null}\n" +
                "\n" +
                "Human{name=:b, surname:y, birthdate=:12/01/2000, iq=:0, schedule=null}\n" +
                "\nChildren:[]" + "\nThis Family has no pet" +
                "\n"
             ;
        assertEquals(expected, family.toString());
        assertNotEquals("Basbfasf", family.toString());
    }
}
