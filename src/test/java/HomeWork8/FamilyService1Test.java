package HomeWork8;

import HomeWork4.*;
import HomeWork4.DAO.CollectionFamilyDao;
import HomeWork4.Service.FamilyService;
import HomeWork4.pets.Dog;
import HomeWork4.pets.Fish;
import HomeWork4.pets.Pet;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FamilyService1Test {
    List<Family> families = new ArrayList<>();
    FamilyService service = new FamilyService(new CollectionFamilyDao(families));
    Human a = new Human();
    Human b = new Human();
    Human c = new Human();
    Human d = new Human();
    Human e = new Human();
    Family f = new Family(a, b);
    Family f2 = new Family(e,c);


    @Test
            public void testGetFamiliesBiggerThan () {
        service.saveFamily(f);
        service.saveFamily(f2);
        List<Family> expected = service.getFamiliesBiggerThan(1);
        List<Family> testList = new ArrayList<>();
        testList.add(f);
        testList.add(f2);
        assertEquals(expected, testList);
    }
    @Test
    public void testGetFamiliesLessThan () {
        service.saveFamily(f);
        service.saveFamily(f2);
        List<Family> expected = service.getFamiliesLessThan(3);
        List<Family> testList = new ArrayList<>();
        testList.add(f);
        testList.add(f2);
        assertEquals(expected, testList);
    }
    @Test
    public void testCountFamiliesWithMemberNumber () {
        service.saveFamily(f);
        service.saveFamily(f2);

        int expected = 2;
        assertEquals(expected, service.countFamiliesWithMemberNumber(2));
    }

    @Test
    public void testCreateNewFamily () {
        service.saveFamily(f);
        service.saveFamily(f2);
        service.createNewFamily(d,e);

        int expected = 3;
        assertEquals(expected, service.count());
    }

    @Test
    public void testBornChild() {
        service.saveFamily(f);
        service.saveFamily(f2);
        service.bornChild(f2, "Katya", "Anton");
        int expected = 3;
        assertEquals(expected, service.getFamilyByIndex(1).countFamily());
    }

    @Test
    public void testAdoptChild() {
        service.saveFamily(f);
        service.adoptChild(f,c, "Bohdan", "Biggy", "01/01/1994", 45);
        int expected = 3;
        assertEquals(expected, service.getFamilyByIndex(0).countFamily());
    }

    @Test
    public void testDeleteAllChildrenOlderThen() {
        service.saveFamily(f);
        e.setBirthDate(LocalDate.of(1994,12,1));
        service.adoptChild(f, c, "Boss", "Bosenko", "01/01/1994", 85);
        service.deleteAllChildrenOlderThen(10);
        int expected = 2;
        assertEquals(expected, service.getFamilyByIndex(0).countFamily());
    }

    @Test
    public void testCount() {
        service.saveFamily(f);
        service.saveFamily(f2);
        int expected = 2;
        assertEquals(expected, service.count());
    }

    @Test
    public void testGetPets() {
        service.saveFamily(f);
        Pet p = new Fish();
        f.addPet(p);
        List<Pet> expected = service.getPets(0);
        List<Pet> testList = new ArrayList<>();
        testList.add(p);
        assertEquals(expected, testList);
    }

    @Test
    public void testAddPet() {
        service.saveFamily(f);
        Pet p = new Fish ();
        Pet s = new Dog();
        f.addPet(p);
        f.addPet(s);
        List<Pet> expected = service.getPets(0);
        List<Pet> testList = new ArrayList<>();
        testList.add(s);
        testList.add(p);
        assertEquals(expected, testList);
    }

    @Test
    public void testSaveFamily() {
        service.saveFamily(f);
        service.saveFamily(f2);
        int expected = service.count();

        assertEquals(expected, 2);
    }

    @Test
    public void testGetAllFamilies() {
        service.saveFamily(f);
        service.saveFamily(f2);
        List<Family> expected = service.getAllFamilies();
        List<Family> testList = new ArrayList<>();
        testList.add(f);
        testList.add(f2);
        assertEquals(expected, testList);
    }

    @Test
    public void testGetFamilyByIndex() {
        service.saveFamily(f);
        service.saveFamily(f2);
        Family expected = service.getFamilyByIndex(1);
        assertEquals(expected, f2);
    }

    @Test
    public void testDeleteFamily() {
        service.saveFamily(f);
        service.saveFamily(f2);
        service.deleteFamily(f2);
        int expected = service.count();
        assertEquals(expected, 1);
    }

}
