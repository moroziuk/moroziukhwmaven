package HomeWork4;

public enum ChildrenNames {
    JENIA{
        @Override
        public String toString() {
            return "Jenia";
        }
    },
    SASHA{
        @Override
        public String toString() {
            return "Sasha";
        }
    },
    MAX{
        @Override
        public String toString() {
            return "Max";
        }
    },
    VALIA{
        @Override
        public String toString() {
            return "Valia";
        }
    },
    SLAVA{
        @Override
        public String toString() {
            return "Slava";
        }
    }
}
