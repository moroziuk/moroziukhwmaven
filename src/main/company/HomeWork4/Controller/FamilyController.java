package HomeWork4.Controller;

import HomeWork4.Family;
import HomeWork4.Human;
import HomeWork4.Service.FamilyService;
import HomeWork4.pets.Pet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class FamilyController implements FamilyControllerDAO {

    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int q) {
        return familyService.getFamiliesBiggerThan(q);
    }

    @Override
    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    @Override
    public List<Family> getFamiliesLessThan (int q) {
        return familyService.getFamiliesLessThan(q);
    }

    @Override
    public int countFamiliesWithMemberNumber (int q) {
        return familyService.countFamiliesWithMemberNumber(q);
    }

    @Override
    public void createNewFamily(Human m, Human f) {
        familyService.createNewFamily(m,f);
    }

    @Override
    public void bornChild (Family f, String girlName, String boyName) throws FamilyOverflowException {
      if (f.countFamily() >= 7) throw new FamilyOverflowException("Family has too much children");
      else familyService.bornChild(f, girlName, boyName);
            }

    @Override
    public void adoptChild(Family f, Human c, String name, String surname, String birthdate, int iq) {
        if (f.countFamily() >= 7) throw new FamilyOverflowException("Family has too much children");
        else familyService.adoptChild(f,c, name, surname, birthdate, iq);
    }

    @Override
    public void deleteAllChildrenOlderThen (int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    @Override
    public int count() {
        return familyService.count();
    }

    @Override
    public List<Pet> getPets (int q) {
        return familyService.getPets(q);
    }

    @Override
    public void addPet (int q, Pet pet) {
        familyService.addPet(q, pet);
    }

    @Override
    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    @Override
    public Family getFamilyByIndex (int r) {
        return familyService.getFamilyByIndex(r);
    }

    @Override
    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    @Override
    public boolean deleteFamily(int d) {
        return familyService.deleteFamily(d);
    }

    @Override
    public void writter () throws IOException {
        familyService.writer();
    }

    public void loadData() throws IOException, ClassNotFoundException {
        familyService.loadData();
    }
}
