package HomeWork4.Controller;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException(String errorMessage) {
        super(errorMessage);
    }
}
