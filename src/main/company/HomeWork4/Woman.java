package HomeWork4;

import HomeWork4.pets.Pet;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public final class Woman extends Human implements HumanCreator, Serializable {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private HashMap<DayOfWeek,String> schedule;

    public Woman() {
    }

    public Woman(String name, String surname, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
    }

    public Woman(String name, String surname, LocalDate birthDate, int iq,
                 Family family, HashMap<DayOfWeek,String> schedule) {
        this.name = name;
        this.surname = surname;
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
        this.iq = iq;
        this.family = family;
        this.schedule = schedule;

        if (iq > 100 || iq < 0) {
            throw new IllegalArgumentException(
                    "iq must be in range 0-100 but found " + iq);}
         }

    public Woman(String name, String surname, LocalDate birthDate, Family family) {
        this.name = name;
        this.surname = surname;
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
        this.family = family;
    }

    public void greetPet(Pet pet) {
        if (family.getPets().contains(pet)) {
            String petNickname = pet.getNickname();
            System.out.printf("Привіт, манюня! Мій ти %s! \n", petNickname);
        }
        else System.out.println("Тю-Тю, це не наша тваринка");
    }

    public void describePet(Pet pet) {if (family.getPets().contains(pet)) {
        String petSpecies = pet.getClass().getSimpleName();
        int petAge = pet.getAge();
        int petTrickLevel = pet.getTrickLevel();
        if (petTrickLevel > 50) {

            System.out.printf(
                    "У мене є %s, йому %d років, він дуже хитрий. \n", petSpecies, petAge);
        } else System.out.printf(
                "У мене є %s, йому %d років, він майже не хитрий\n", petSpecies, petAge);
    }
    else System.out.println ("Не знаю такого улюбленця");}

    public String describeAge() {

        LocalDate now = LocalDate.now();
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        LocalDate birth = Instant.ofEpochMilli(birthDate).atZone(zoneId).toLocalDate();

        Period period = Period.between(birth, now);
        return String.format("%d days, %d months, %d  years",
                period.getDays(), period.getMonths(), period.getYears());
    }

    public int getAge() {
        LocalDate now = LocalDate.now();
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        LocalDate birth = Instant.ofEpochMilli(birthDate).atZone(zoneId).toLocalDate();

        Period period = Period.between(birth, now);
        return period.getYears();
    }

    public Human bornChild(Man a, Woman b) {
        ChildrenNames[] names = ChildrenNames.values();
        Random random = new Random();

        if ( (int) (Math.random()+0.5) == 1) {
            Human child = new Man();
            child.setName(names[random.nextInt(names.length)].toString());
            child.setSurname(a.getSurname());
            child.setFamily(a.getFamily());
            child.setIq((a.getIq()+b.getIq())/2);
            a.getFamily().addChild(child);
            return child;
        }
else {
            Human child = new Woman();
            child.setName(names[random.nextInt(names.length)].toString());
            child.setSurname(a.getSurname());
            child.setFamily(a.getFamily());
            child.setIq((a.getIq()+b.getIq())/2);
            a.getFamily().addChild(child);
            return child;
        }
    }

    public  String getName() {
        return this.name;
    }

    public  void setName(String name) {
        this.name = name;
    }

    public HashMap<DayOfWeek,String> getSchedule() {
        return schedule;
    }

    public void setSchedule(HashMap<DayOfWeek,String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    @Override
    public long getBirthDate() {
        return birthDate;
    }

    public LocalDate getBirthLocalDate() {
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        return Instant.ofEpochMilli(birthDate).atZone(zoneId).toLocalDate();
    }

    @Override
    public void setBirthDate(LocalDate birthDate) {
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
    }

    public  String getSurname() {
        return surname;
    }

    public  void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        LocalDate birth = Instant.ofEpochMilli(getBirthDate()).atZone(zoneId).toLocalDate();
        String birthFormatted = birth.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return String.format("Human{name=:%s, surname:%s, birthdate=:%s, iq=:%s, schedule=%s}\n",
                getName(),
                getSurname(),
                birthFormatted,
                getIq(),
                getSchedule()
        );
    }

    public String prettyFormat() {
        return new StringBuilder()
                .append("{name=")
                .append(name)
                .append(", surname=")
                .append(surname)
                .append(", birthDate=")
                .append(getBirthLocalDate())
                .append(", iq=")
                .append(iq)
                .append(", schedule=")
                .append(schedule)
                .append("}")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Woman woman = (Woman) o;
        if (schedule == null) {
            return birthDate == woman.birthDate && iq == woman.iq;
        }
        else return birthDate == woman.birthDate && iq == woman.iq && schedule.equals(woman.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(birthDate, iq, surname);
        result = 31 * result;
        return result;
    }
}
