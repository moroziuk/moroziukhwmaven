package HomeWork4;

import HomeWork4.pets.Pet;

import java.io.Serializable;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Human implements Serializable{

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private HashMap<DayOfWeek,String> schedule;

    public Human() {
    }

    public Human(String name, String surname, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
    }

    public Human(String name, String surname, LocalDate birthDate, int iq,
                 Family family, HashMap<DayOfWeek,String> schedule) {
        this.name = name;
        this.surname = surname;
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
        this.iq = iq;
        this.family = family;
        this.schedule = schedule;

        if (iq > 100 || iq < 0) {
            throw new IllegalArgumentException(
                    "iq must be in range 0-100 but found " + iq);}
    }

    public Human(String name, String surname, LocalDate birthDate, Family family) {
        this.name = name;
        this.surname = surname;
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
        this.family = family;
    }

    public static Human randomizerHuman() {
        double r1 = Math.random();
        double r2 = Math.random();
        double r3 = Math.random();
        return new Human(
                r1 < 0.4 ? "Sasha" : r1 < 0.6 ? "Petro" : "Vasyl",
                r2 < 0.5 ? "Bondar" : "Koval",
                r3 < 0.4? LocalDate.of(1994, 1, 1) :
                r3 < 0.6? LocalDate.of(2004, 2, 2) :
                r3 < 0.7? LocalDate.of(2014, 3, 3) :
                LocalDate.of(2020, 4, 4)
        );
    }

    public void greetPet(Pet pet) {
        if (family.getPets().contains(pet)) {
            String petNickname = pet.getNickname();
            System.out.printf("Привіт, %s! \n", petNickname);
        }
        else System.out.println("Тю-Тю, це не наша тваринка");

    }
    public void describePet(Pet pet) {
        if (family.getPets().contains(pet)) {
            String petSpecies = pet.getClass().getSimpleName();
            int petAge = pet.getAge();
            int petTrickLevel = pet.getTrickLevel();
            if (petTrickLevel > 50) {

                System.out.printf(
                        "У мене є %s, йому %d років, він дуже хитрий. \n", petSpecies, petAge);
            } else System.out.printf(
                    "У мене є %s, йому %d років, він майже не хитрий\n", petSpecies, petAge);
        }
        else System.out.println ("Не знаю такого улюбленця");
    }

    public String describeAge() {

        LocalDate now = LocalDate.now();
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        LocalDate birth = Instant.ofEpochMilli(birthDate).atZone(zoneId).toLocalDate();

        Period period = Period.between(birth, now);
        return String.format("%d days, %d months, %d  years",
                  period.getDays(), period.getMonths(), period.getYears());
    }

    public int getAge() {
        LocalDate now = LocalDate.now();
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        LocalDate birth = Instant.ofEpochMilli(birthDate).atZone(zoneId).toLocalDate();

        Period period = Period.between(birth, now);
        return period.getYears();
    }

    public  String getName() {
        return this.name;
    }

    public  void setName(String name) {
        this.name = name;
    }

    public HashMap<DayOfWeek,String> getSchedule() {
        return this.schedule;
    }

    public void setSchedule(HashMap<DayOfWeek,String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return this.family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public int getIq() {
        return this.iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public long getBirthDate() {
        return this.birthDate;
    }

    public LocalDate getBirthLocalDate() {
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        return Instant.ofEpochMilli(birthDate).atZone(zoneId).toLocalDate();
    }

    public void setBirthDate(LocalDate birthDate) {
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        this.birthDate = birthDate.atStartOfDay(zoneId).toInstant().toEpochMilli();
    }

    public  String getSurname() {
        return this.surname;
    }

    public  void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        ZoneId zoneId = ZoneId.of("Europe/Kiev");
        LocalDate birth = Instant.ofEpochMilli(getBirthDate()).atZone(zoneId).toLocalDate();
        String birthFormatted = birth.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return String.format("Human{name=:%s, surname:%s, birthdate=:%s, iq=:%s, schedule=%s}\n",
                getName(),
                getSurname(),
                birthFormatted,
                getIq(),
                getSchedule()
        );
    }

    public String prettyFormat() {
        return new StringBuilder()
                .append("{name=")
                .append(name)
                .append(", surname=")
                .append(surname)
                .append(", birthDate=")
                .append(getBirthLocalDate())
                .append(", iq=")
                .append(iq)
                .append(", schedule=")
                .append(schedule)
                .append("}")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        if (schedule == null) {
        return birthDate == human.birthDate && iq == human.iq;
    }
        else return birthDate == human.birthDate && iq == human.iq && schedule.equals(human.schedule);
    }


    @Override
    public int hashCode() {
        int result = Objects.hash(birthDate, iq, surname);
        result = 31 * result;
        return result;
    }

}
