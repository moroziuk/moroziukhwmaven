package HomeWork4.Service;

public class FamilyAdoptionDateException extends RuntimeException {
    public FamilyAdoptionDateException(String errorMessage) {
        super(errorMessage);
    }
}
