package HomeWork4.Service;

import HomeWork4.Controller.FamilyOverflowException;
import HomeWork4.DAO.CollectionFamilyDao;
import HomeWork4.Family;
import HomeWork4.Human;
import HomeWork4.pets.Pet;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService implements FamilyServiceDAO {

    private final CollectionFamilyDao collectionFamilyDAO;

    public FamilyService(CollectionFamilyDao collectionFamilyDAO) {
        this.collectionFamilyDAO = collectionFamilyDAO;
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int q) {
        List<Family> storage = collectionFamilyDAO.getAllFamilies();
        List<Family> biggerFamilies =
                storage.stream()
                        .filter(f -> f.countFamily() > q)
                        .collect(Collectors.toList());
        biggerFamilies
                .forEach(i -> System.out.println(i.prettyFormat()));
        return biggerFamilies;
    }

    @Override
    public void displayAllFamilies() {
        List<Family> storage = collectionFamilyDAO.getAllFamilies();
        storage.forEach(i -> System.out.println(i.prettyFormat()));
    }

    @Override
    public List<Family> getFamiliesLessThan (int q) {
        List<Family> storage = collectionFamilyDAO.getAllFamilies();
        List<Family> lesserFamilies =
                storage.stream()
                        .filter(f -> f.countFamily() < q)
                        .collect(Collectors.toList());
        lesserFamilies
                .forEach(i -> System.out.println(i.prettyFormat()));
        return lesserFamilies;
    }

    @Override
    public int countFamiliesWithMemberNumber (int q) {
        List<Family> storage = collectionFamilyDAO.getAllFamilies();
        long count =
                storage.stream()
                        .filter(f -> f.countFamily() == q)
                        .count();
        return (int) count;
    }

    @Override
    public void createNewFamily(Human m, Human f) {
         Family family = new Family(m, f);
        collectionFamilyDAO.saveFamily(family);
    }

    public Family bornChild (Family f, String girlName, String boyName) {
        return collectionFamilyDAO.bornChild(f, girlName, boyName);
    }

    public Family adoptChild(Family f, Human c, String name, String surname, String birthdate, int iq) {
        try {
            return collectionFamilyDAO.adoptChild(f,c, name, surname, birthdate, iq);
        } catch (FamilyOverflowException | ParseException e) {
            throw new FamilyOverflowException("Birthdate is wrong");
        }
    }
    public void deleteAllChildrenOlderThen (int age ) {
        collectionFamilyDAO.deleteAllChildrenOlderThen(age);
    }

    @Override
    public int count() {
        return collectionFamilyDAO.getAllFamilies().size();
    }

    @Override
    public List<Pet> getPets (int q) {
        List<Family> storage = collectionFamilyDAO.getAllFamilies();
         List<Pet> pets = new ArrayList<>();
            if (storage.size()>q) {
                ArrayList<Pet> petsAdded = new ArrayList<>(storage.get(q).getPets());
                pets.addAll(petsAdded);
            }
            return pets;
    }

    public void addPet (int q, Pet pet) {
        collectionFamilyDAO.addPet(q, pet);
    }

    public void saveFamily(Family family) {
        collectionFamilyDAO.saveFamily(family);
    }

    public List<Family> getAllFamilies() {
        return collectionFamilyDAO.getAllFamilies();
    }

    public Family getFamilyByIndex (int r) {
        return collectionFamilyDAO.getFamilyByIndex(r);
    }

    public boolean deleteFamily(Family family) {
        return collectionFamilyDAO.deleteFamily(family);
    }

    public boolean deleteFamily(int d) {
        return collectionFamilyDAO.deleteFamily(d);
    }

    public void writer () throws IOException {
        FileOutputStream outputStream = new FileOutputStream("families.ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(collectionFamilyDAO.getAllFamilies());
        objectOutputStream.close();
    }

    public void loadData() throws IOException, ClassNotFoundException {
        // якщо цей метод завантажує дані з файлу - навіщо давати йому приймати List<Family>??
        collectionFamilyDAO.loadData(CollectionFamilyDao.readFamiliesFromFile());
    };

}

