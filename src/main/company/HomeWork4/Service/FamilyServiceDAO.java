package HomeWork4.Service;

import HomeWork4.Family;
import HomeWork4.Human;
import HomeWork4.pets.Pet;

import java.time.LocalDate;
import java.util.List;

public interface FamilyServiceDAO {

    List<Family> getFamiliesBiggerThan(int q);

    void displayAllFamilies();

    List<Family> getFamiliesLessThan (int q);

    int countFamiliesWithMemberNumber (int q);

    void createNewFamily(Human m, Human f);

     Family bornChild (Family f, String girlName, String boyName);

     Family adoptChild(Family f, Human c, String name, String surname, String birthdate, int iq);

     void deleteAllChildrenOlderThen (int age );

     int count();

     List<Pet> getPets (int q);

     void addPet (int q, Pet pet);

     void saveFamily(Family family);

     List<Family> getAllFamilies();

     Family getFamilyByIndex (int r);

     boolean deleteFamily(Family family);

     boolean deleteFamily(int d);

}
