package HomeWork4.pets;

public class Fish extends Pet {

    public Fish() {
    }

    public Fish(String nickname, int age, int trickLevel, String... habits) {

        super(nickname, age, trickLevel, habits);

        if (trickLevel > 100 || trickLevel < 0) {
            throw new IllegalArgumentException(
                    "trickLevel must be in range 0-100 but found " + trickLevel);}
        }

    public void respond() {};


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return String.format("%s:{nickname=:%s, age:%s, trickLevel=:%s,habits=:%s}\n",
                Species.FISH,
                getNickname(),
                getAge(),
                getTrickLevel(),
                getHabits()
        );
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}







