package HomeWork4.pets;

public class Dog extends Pet implements Foul {

    public Dog() {
    }

    public Dog(String nickname) {
    super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String... habits) {

        super(nickname, age, trickLevel, habits);

        if (trickLevel > 100 || trickLevel < 0) {
            throw new IllegalArgumentException(
                    "trickLevel must be in range 0-100 but found " + trickLevel);}
        }

    public void respond() {
        System.out.printf("Гав! Я - %s.Я ДУЖЕ РАДИЙ, ЩО ТИ ПРИЙШОВ! \n", getNickname());
    };

    public void foul() {
        System.out.println("Я з'їв усі докази");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return String.format("%s:{nickname=:%s, age:%s, trickLevel=:%s,habits=:%s}\n",
                Species.DOG,
                getNickname(),
                getAge(),
                getTrickLevel(),
                getHabits()
        );
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}







