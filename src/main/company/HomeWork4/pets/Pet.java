package HomeWork4.pets;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;

public abstract class Pet implements Serializable {
    private String nickname;
    private int age;
    private int trickLevel;
    private HashSet<String> habits;

    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    private static HashSet<String> combineHabits(String... habits) {
        return new HashSet<>(Arrays.asList(habits));
    }

    public Pet(String nickname, int age, int trickLevel, String... habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = combineHabits(habits);

        if (trickLevel > 100 || trickLevel < 0) {
            throw new IllegalArgumentException(
                    "trickLevel must be in range 0-100 but found " + trickLevel);
        }
    }


    public void eat() {
        System.out.println("Я ї'м!");
    }

    abstract void respond();

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public HashSet<String> getHabits() {
        return habits;
    }

    public void setHabits(HashSet<String> habits) {
        this.habits = habits;
    }
    public String prettyFormat() {
        return new StringBuilder()
                .append("{species=")
                .append(getClass().getSimpleName())
                .append(", nickname=")
                .append(nickname)
                .append(", age=")
                .append(age)
                .append(", trickLevel=")
                .append(trickLevel)
                .append(", habits=")
                .append(habits)
                .append("}")
                .toString();
    }
}

