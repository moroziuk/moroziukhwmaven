package HomeWork4.pets;

public class RoboCat extends Pet implements Foul {

    public RoboCat() {
    }

    public RoboCat(String nickname) {
    super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {

        super(nickname, age, trickLevel, habits);

        if (trickLevel > 100 || trickLevel < 0) {
            throw new IllegalArgumentException(
                    "trickLevel must be in range 0-100 but found " + trickLevel);}
        }

    public void eat() {
        System.out.println("МММ бездротова зарядка, мій улюблений смак!");
    };

    public void respond() {
        System.out.printf("Мя-у! Я - %s. Передаю масло. \n", getNickname());
    };

    public void foul() {}

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return String.format("%s:{nickname=:%s, age:%s, trickLevel=:%s,habits=:%s}\n",
                Species.ROBOCAT,
                getNickname(),
                getAge(),
                getTrickLevel(),
                getHabits()
        );
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}







