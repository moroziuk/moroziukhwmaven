package HomeWork4.pets;

public enum Species {
    FISH {
        @Override
        public String toString() {
            return "Fish";
        }
    },
    DOMESTICCAT{
        @Override
        public String toString() {
            return "DomesticCat";
        }
    },
    DOG {
        @Override
        public String toString() {
            return "Dog";
        }
    },
    ROBOCAT{
        @Override
        public String toString() {
            return "RoboCat";
        }
    },
    UNKNOWN {
        @Override
        public String toString() {
            return "Unknown";
        }
    }
}
