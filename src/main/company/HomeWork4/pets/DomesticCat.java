package HomeWork4.pets;

public class DomesticCat extends Pet implements Foul {

    public DomesticCat() {
    }

    public DomesticCat(String nickname) {
    super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String... habits) {

        super(nickname, age, trickLevel, habits);

        if (trickLevel > 100 || trickLevel < 0) {
            throw new IllegalArgumentException(
                    "trickLevel must be in range 0-100 but found " + trickLevel);}
        }

    public void respond() {
        System.out.printf("Мняв! Я - %s. Сплю. \n", getNickname());
    };

    public void foul() {
        System.out.println("Загрібаю лапами пісочок");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return String.format("%s:{nickname=:%s, age:%s, trickLevel=:%s,habits=:%s}\n",
                Species.DOMESTICCAT,
                getNickname(),
                getAge(),
                getTrickLevel(),
                getHabits()
        );
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}







