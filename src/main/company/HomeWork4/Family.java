package HomeWork4;

import HomeWork4.pets.Pet;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class Family implements Serializable {
        private Human mother;
        private Human father;
        private ArrayList<Human> children;
        private HashSet<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        mother.setFamily(this);
        this.father = father;
        father.setFamily(this);
        children = new ArrayList<>();
        pets = new HashSet<>();
    }

    public ArrayList<Human> getChildren() {
        return this.children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Human getFather() {
        return this.father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return this.mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public HashSet<Pet> getPets() {
        return this.pets;
    }

    public void setPets(HashSet<Pet> pets) {
        this.pets = pets;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    public void deletePet (Pet pet) {
        this.pets.remove(pet);
    }

     public void addChild(Human child) {
                child.setFamily(this);
                children.add(child);
    }

    public static Family randomizerFamily() {
        return new Family(
                Human.randomizerHuman(),
                Human.randomizerHuman()
        );
    }
        public void randomizerFamilyChildren() {

            double r1 = Math.random();
               if (r1 < 0.4) {
                   children.add(Human.randomizerHuman());
               }
               else if (r1 < 0.6) {
                   children.add(Human.randomizerHuman());
                   children.add(Human.randomizerHuman());
               }
               else {
                   children.add(Human.randomizerHuman());
                   children.add(Human.randomizerHuman());
                   children.add(Human.randomizerHuman());
               }
             }

    public void deleteChild (Human child) {
        if (children.contains(child)) {
            children.remove(child);
        }
    }
    public boolean deleteChild (int index) {
    if (children.size()>index) {
        children.remove(index);
        return true;
    }
     return false;
    }

    public int countFamily () {
        return 2+children.size();
    }

    @Override
    public String toString() {
        if (!pets.isEmpty()) {
            return String.format(

                            getMother().toString() +
                            "\n" +
                            getFather().toString() +
                            "\nChildren:" +
                            getChildren() + getPets().toString() +
                            "\n"
            );
        }
        else return String.format(
                        getMother().toString() +
                        "\n" +
                        getFather().toString() +
                        "\nChildren:" +
                        getChildren() + "\nThis Family has no pet" +
                                "\n"
        );
    }

    public static String childrenPrettyF(ArrayList<Human> a) {

        if (a.isEmpty()) return "           Childfree\n";

        StringBuilder b = new StringBuilder();

        for (int i = 0; ; i++) {
            if (a.get(i) instanceof Man) {
                b.append("           boy: ");
            }
            else if (a.get(i) instanceof Woman) {
                b.append("           girl: ");
            }
            else b.append("           unknown sex: ");
            b.append(a.get(i).prettyFormat());
            b.append("\n");
            if (i == a.size()-1)
                return b.toString();
        }
    }

    public static String petsPrettyF(HashSet<Pet> p) {

        if (p.isEmpty()) return "  Petsfree\n";
        List<Pet> a = new ArrayList<>(p);
        StringBuilder b = new StringBuilder();

        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a.get(i).prettyFormat());
            b.append(", ");
            if (i == a.size()-1)
                return b.append("]\n").toString();
        }
    }

    public String prettyFormat() {
        return new StringBuilder()
                .append("family:\n")
                .append("   mother: ")
                .append(mother.prettyFormat())
                .append("\n   father: ")
                .append(father.prettyFormat())
                .append("\n   children:\n")
                .append(childrenPrettyF(children))
                .append("   pets: ")
                .append(petsPrettyF(pets))
                .toString();
    }


/*    public static Family deserialize(String sF) throws ParseException {
        String motherS = sF.substring(sF.indexOf("mother:"), sF.indexOf("father:"));
        String fatherS = sF.substring(sF.indexOf("father:"), sF.indexOf("children:"));
        String childrenS = sF.substring(sF.indexOf("children:"), sF.indexOf("pets:"));
        String petsS = sF.substring(sF.indexOf("pets:"));

        String[] motherParts = motherS.split(", ");
        String[] fatherParts = fatherS.split(", ");
        String[] childrenParts = childrenS.split(", ");
        String[] petsParts = petsS.split(", ");

        String mName = motherParts[0].substring(motherParts[0].indexOf('=') + 1);
        String mSurname = motherParts[1].substring(motherParts[1].indexOf('=') + 1);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date dm = df.parse(motherParts[2].substring(motherParts[2].indexOf('=') + 1));
        LocalDate mBD = dm.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        int mIq = Integer.parseInt(motherParts[3].substring(motherParts[3].indexOf('=') + 1));
        String[] mScheduleParts = motherParts[4].substring(motherParts[4].indexOf('{') + 1, motherParts[4].indexOf('}')).split(",");
        Arrays.stream(mScheduleParts).forEach(p -> p.substring(0, p.indexOf('=')));

        HashMap<DayOfWeek,String> mSchedule = motherParts[4].
                .substring(motherParts[4].indexOf('=') + 1);

        Human mother = new Human(motherParts[0], motherParts[1], motherParts)
        String mother = parts[0].substring(parts[0].indexOf('{') + 1);
        String father = parts[1].substring(parts[1].indexOf('{') + 1);
        String father = parts[1].substring(parts[1].indexOf('{') + 1);
        int id = Integer.parseInt(parts[0].substring(parts[0].indexOf('=') + 1));
        int flightId = Integer.parseInt(parts[1].substring(parts[1].indexOf('=') + 1));
        String passenger =  parts[2].substring(parts[2].indexOf('=') + 1, parts[2].length() - 1);
        List<String> passengers = new ArrayList<> (Arrays.asList(passenger.split("_ ")));

        return new Family( );
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(pets, family.pets) && Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && children.equals(family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(pets, mother, father);
        result = 31 * result + children.hashCode();
        return result;
    }
}
