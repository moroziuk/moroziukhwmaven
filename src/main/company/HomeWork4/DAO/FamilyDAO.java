package HomeWork4.DAO;

import HomeWork4.Family;

import java.util.List;

public interface FamilyDAO {
    List<Family> getAllFamilies();
    Family getFamilyByIndex (int r);
    boolean deleteFamily(Family t);
    boolean deleteFamily(int d);
    void saveFamily(Family t);
}