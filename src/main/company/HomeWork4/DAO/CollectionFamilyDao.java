package HomeWork4.DAO;

import HomeWork4.Family;
import HomeWork4.Human;
import HomeWork4.Man;
import HomeWork4.Woman;
import HomeWork4.pets.Pet;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class
CollectionFamilyDao implements FamilyDAO {

    private List<Family> storage;

    public CollectionFamilyDao(List<Family> storage) {
        this.storage = storage;
    }

    public Family bornChild (Family f, String girlName, String boyName) {

        if ( (int) (Math.random()+0.5) == 1) {
            Human child = new Man();
            child.setName(boyName);
            child.setSurname(f.getFather().getSurname());
            child.setFamily(f);
            f.addChild(child);
        }
        else {
            Human child = new Woman();
            child.setName(girlName);
            child.setSurname(f.getFather().getSurname());
            child.setFamily(f);
            f.addChild(child);
        }
        storage.set(storage.indexOf(f), f);
        return f;
    }

    public Family adoptChild(Family f, Human c, String name, String surname, String birthdate, int iq) throws ParseException {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date d1 = df.parse(birthdate);
        LocalDate birthDate = d1.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        c.setSurname(surname);
        c.setName(name);
        c.setIq(iq);
        c.setBirthDate(birthDate);
        f.addChild(c);

        storage.set(storage.indexOf(f), f);
        return f;
    }

   public void deleteAllChildrenOlderThen (int age) {
       for (Family childAll: storage){
           childAll.getChildren().removeIf(c -> c.getAge() >= age);
       }
   }

    public void addPet (int q, Pet pet) {
        if (storage.size()>q) {
            storage.get(q).addPet(pet);
            storage.set(q, storage.get(q));
        }
    }

    public void loadData(List<Family> families) {
        storage = families;
    }

    public static List<Family> readFamiliesFromFile() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(new File("families.ser"));
        ObjectInputStream stream = new ObjectInputStream(fileInputStream);
        List<Family> families = (ArrayList<Family>) stream.readObject();
        stream.close();
        return families;
    }

    @Override
    public void saveFamily(Family family) {

        if (storage.contains(family)) {
            storage.set(storage.indexOf(family), family);
        }
        storage.add(family);
    }

    @Override
    public List<Family> getAllFamilies() {
        return storage;
    }

    @Override
    public Family getFamilyByIndex (int r) {
        if (storage.get(r) != null) {
            return storage.get(r);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (storage.contains(family)) {
            storage.remove(family);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(int d) {
        if (storage.size()>d) {
            storage.remove(d);
            return true;
        }
        return false;
    }
}