package HomeWork4;

import HomeWork4.Controller.FamilyController;
import HomeWork4.Controller.FamilyOverflowException;
import HomeWork4.DAO.CollectionFamilyDao;
import HomeWork4.Service.FamilyService;
import HomeWork4.pets.Dog;
import HomeWork4.pets.DomesticCat;
import HomeWork4.pets.Fish;
import HomeWork4.pets.Pet;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.*;

public class Main {

    static void fill(HashMap<DayOfWeek,String> data) {
        data.put(DayOfWeek.SUNDAY, "coding");
        data.put(DayOfWeek.MONDAY, "football");
        data.put(DayOfWeek.TUESDAY, "parenting");
        data.put(DayOfWeek.WEDNESDAY, "swimming");
        data.put(DayOfWeek.THURSDAY, "cooking");
        data.put(DayOfWeek.FRIDAY, "joking");
        data.put(DayOfWeek.SATURDAY, "basketball");
    }
    static HashMap<DayOfWeek,String> calendar = new HashMap<>();
    static ArrayList<Human> garbage = new ArrayList<>(1_000_000);
    static void fillHuman (ArrayList<Human> data) {

        for (int i = 0; i<100_000; i++) {
           Human b = new Human();
            data.add(b);
        };
    };

    static boolean isInt(String raw) {
        try {
            int n = Integer.parseInt(raw);
            return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    static int toInt(String raw) {
        return Integer.parseInt(raw);
    }

    static boolean validate(int x, int y ) {
        return x > 0 && x < y;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        fill(calendar);
        fillHuman(garbage);

        Pet pet1 = new Dog();
        pet1.setNickname("Woofer");
        Pet pet2 = new DomesticCat
                ("Lem");
        Pet pet3 = new Fish("Swimmer", 12, 55,"swimming","drowning");
        Pet pet4 = new Fish("Body", 12, 55,"swimming","drowning");

        Man sashko = new Man
                ("Sashko", "Moroziuk", LocalDate.of(1994, 12, 7));
        Woman yulia = new Woman();
        yulia.setName("Yulia");
        yulia.setSurname("Moroziuk");

        Human human = new Human("Antonia", "Hoochi", LocalDate.of(1996, 12, 10));

        Family moroziuk = new Family
                (yulia, sashko);

        Woman anna = new Woman
                ("Anna", "Korol", LocalDate.of(1984, 2, 11),
                        55, moroziuk, calendar);
        Man ostap = new Man
                ("Ostap", "Korol", LocalDate.of(1985, 3, 15), moroziuk);

        Family korol = new Family
                (anna, ostap);
        Human andrii = new Man
                ("Andrii", "Moroziuk", LocalDate.of(2000, 11, 25),
                        99, moroziuk, calendar);
        Human bodya = new Man( "Bodia", "Bondar", LocalDate.of(2000, 6, 16));
        Human lisa = new Woman( "Lisa", "Bondar", LocalDate.of(2018, 5, 20));
        Human tonya = new Woman( "Tonya", "Moroziuk", LocalDate.of(2020, 6, 15));

        moroziuk.addChild(andrii);
        moroziuk.addPet(pet3);
        moroziuk.addPet(pet4);
        yulia.setFamily(moroziuk);
        sashko.setFamily(moroziuk);
        korol.addPet(pet2);
        Human b = anna.bornChild(ostap, anna);
        korol.addChild(lisa);
        korol.addChild(bodya);

      System.out.println(bodya.getBirthDate());
      System.out.println(bodya.describeAge());
      System.out.println(LocalDate.now());
      System.out.println(korol);

List<Family> families = new ArrayList<>();
families.add(moroziuk);
families.add(korol);
FamilyController controller = new FamilyController(new FamilyService(new CollectionFamilyDao(families)));

        String mainMenu =
                "- 1. Завантажити дані з файлу\n" +
                        "- 2. Відобразити весь список сімей (відображає список усіх сімей з індексацією, що починається з 1)\n" +
                        "- 3. Відобразити список сімей, де кількість людей більша за задану\n" +
                        "- 4. Відобразити список сімей, де кількість людей менша за задану\n" +
                        "- 5. Підрахувати кількість сімей, де кількість членів дорівнює заданій\n" +
                        "- 6. Створити нову родину\n" +
                        "- 7. Видалити сім'ю за індексом сім'ї у загальному списку\n" +
                        "- 8. Редагувати сім'ю за індексом сім'ї у загальному списку\n" +
                        "- 9. Видалити всіх дітей старше віку (у всіх сім'ях видаляються діти старше зазначеного віку - вважатимемо, що вони виросли)\n" +
                        "- 10. Зберегти дані в файл\n";

        String menu345 =
                "   - 1 введіть кількість людей";
        String menu8Lower =
                "   - 1. Народити дитину\n" +
                        "   - 2. Усиновити дитину\n" +
                        "   - 3. Повернутися до головного меню\n";

        InputStream in = System.in;
        Scanner sc = new Scanner(in);

        System.out.print(mainMenu);

        while (true) {

            String s = sc.nextLine();
            int x;

            if (isInt(s)) {
                x = toInt(s);
                if (!(validate(x, 11))) System.out.println("Будь ласка, виберіть доступну опцію");

                else if (x == 1) {
                    controller.loadData();
                    System.out.println("Дані завантажено з файлу");
                    System.out.println(mainMenu);
                }

                else if (x == 2) {
                    controller.displayAllFamilies();
                    System.out.print(mainMenu);
                }
                else if (x == 3) {
                    while (true) {
                        System.out.println(menu345);
                        String str = sc.nextLine();
                        if (isInt(str)) {
                            int q = toInt(str);
                            controller.getFamiliesBiggerThan(q);
                            System.out.print(mainMenu);
                            break;
                        }
                        else System.out.println("Будь ласка, введіть номер");
                    }
                }
                else if (x == 4) {
                    while (true) {
                        System.out.println(menu345);
                        String str = sc.nextLine();
                        if (isInt(str)) {
                            int q = toInt(str);
                            controller.getFamiliesLessThan(q);
                            System.out.print(mainMenu);
                            break;
                        }
                        else System.out.println("Будь ласка, введіть номер");
                    }
                }
                else if (x == 5) {
                    while (true) {
                        System.out.println(menu345);
                        String str = sc.next();
                        if (isInt(str)) {
                            int q = toInt(str);
                            System.out.println(controller.countFamiliesWithMemberNumber(q)) ;
                                System.out.print(mainMenu);
                                break;
                            }
                        else System.out.println("Будь ласка, введіть номер");
                        }
                    }

                else if (x == 6) {

                    System.out.println("Введіть ім'я матері");
                    String motherName = sc.next();

                    System.out.println("Введіть прізвище матері");
                    String motherSurName = sc.next();

                    int mYob = 0;
                    do {
                        System.out.println("Введіть рік народження матері");
                        String motherYob = sc.next();
                        if (isInt(motherYob)) {
                            mYob = toInt(motherYob);
                        }
                        if (validate(mYob, LocalDate.now().getYear() + 1)) {
                            break;
                        }
                        else System.out.println("Введіть правильне число");
                    }
                    while (true);

                    int mMob;
                    while (true) {
                        System.out.println("Введіть місяць народження матері");
                        String motherMob = sc.next();
                        if (isInt(motherMob)) {
                            mMob = toInt(motherMob);
                            if (validate(mMob, 13)) {
                                break;
                            }
                        }
                    }
                    int mDob;
                    while (true) {
                        System.out.println("Введіть день народження матері");
                        String motherDob = sc.next();
                        if (isInt(motherDob)) {
                            mDob = toInt(motherDob);
                            YearMonth yearMonthObject = YearMonth.of(mYob, mMob);
                            int daysInMonth = yearMonthObject.lengthOfMonth();
                            if (validate(mDob, daysInMonth+1)) {
                                break;
                            }
                        }
                    }
                    int mIQ;
                    while (true) {
                        System.out.println("Введіть IQ матері");
                        String motherIQ = sc.next();
                        if (isInt(motherIQ)) {
                            mIQ = toInt(motherIQ);
                            if (validate(mIQ, 101)) {
                                break;
                            }
                        }
                    }

                    System.out.println("Введіть ім'я батька");
                    String fatherName = sc.next();

                    System.out.println("Введіть прізвище батька");
                    String fatherSurName = sc.next();

                    int fYob;
                    while (true) {
                        System.out.println("Введіть рік народження батька");
                        String fatherYob = sc.next();
                        if (isInt(fatherYob)) {
                            fYob = toInt(fatherYob);
                            if (validate(fYob, LocalDate.now().getYear()+1)) {
                                break;
                            }
                        }
                    }

                    int fMob;
                    while (true) {
                        System.out.println("Введіть місяць народження батька");
                        String fatherMob = sc.next();
                        if (isInt(fatherMob)) {
                            fMob = toInt(fatherMob);
                            if (validate(fMob, 13)) {
                                break;
                            }
                        }
                    }

                    int fDob;
                    while (true) {
                        System.out.println("Введіть день народження батька");
                        String fatherDob = sc.next();
                        if (isInt(fatherDob)) {
                            fDob = toInt(fatherDob);
                            YearMonth yearMonthObject = YearMonth.of(fYob, fMob);
                            int daysInMonth = yearMonthObject.lengthOfMonth();
                            if (validate(fDob, daysInMonth+1)) {
                                break;
                            }
                        }
                    }

                    int fIQ;
                    while (true) {
                        System.out.println("Введіть IQ батька");
                        String fatherIQ = sc.next();
                        if (isInt(fatherIQ)) {
                            fIQ = toInt(fatherIQ);
                            if (validate(fIQ, 101)) {
                                break;
                            }
                        }
                    }
                    Man father = new Man (fatherName, fatherSurName, LocalDate.of(fYob,fMob,fDob));
                    father.setIq(fIQ);
                    Woman mother = new Woman(motherName, motherSurName, LocalDate.of(mYob, mMob, mDob));
                    mother.setIq(mIQ);
                    controller.createNewFamily(mother, father);
                    System.out.print(mainMenu);
                }

                else if (x == 7) {
                    while (true) {
                        System.out.println("Введіть порядковий номер сім'ї (ID)");
                        String str = sc.next();
                        if (isInt(str)) {
                            int q = toInt(str);
                            if (q >=0 && q< controller.count()) {
                                controller.deleteFamily(q);
                                System.out.print(mainMenu);
                                break;
                            } else System.out.println("Такого номеру немає в списку");
                            if (controller.count() == 0) throw new RuntimeException("Список порожній");
                        }
                    }
                }
                else if (x == 8) {
                    while (true) {
                        System.out.println(menu8Lower);
                        String str = sc.next();
                        if (isInt(str)) {
                            int q = toInt(str);
                            if (validate(q, 4)) {

                                if (q == 1) {
                                    int ID;
                                    while (true) {
                                        System.out.println("Введіть порядковий номер сім'ї");
                                        String strID = sc.next();
                                        if (isInt(strID)) {
                                            ID = toInt(strID);
                                            if (ID >=0 && ID< controller.count()) {
                                                break;
                                            }
                                        }
                                    }
                                    System.out.println("Введіть ім'я для хлопчика");
                                    String boyName = sc.next();

                                    System.out.println("Введіть ім'я для дівчинки");
                                    String girlName = sc.next();

                                    try {
                                        controller.bornChild(controller.getFamilyByIndex(ID), girlName, boyName);
                                    }
                                    catch (FamilyOverflowException ex) {
                                        System.out.println(ex.getMessage());
                                    }
                                    System.out.println(mainMenu);
                                    break;
                                }

                                else if (q == 2) {

                                    int ID;
                                    while (true) {
                                        System.out.println("Введіть порядковий номер сім'ї");
                                        String strID = sc.next();
                                        if (isInt(strID)) {
                                            ID = toInt(strID);
                                            if (ID >=0 && ID< controller.count()) {
                                                break;
                                            }
                                        }
                                    }
                                    System.out.println("Введіть ім'я дитини");
                                    String childName = sc.next();

                                    System.out.println("Введіть прізвище дитини");
                                    String childSurname = sc.next();

                                    System.out.println("Введіть дату народження в форматі dd/MM/yyyy");
                                    String birthDate = sc.next();

                                    int cIQ;
                                    while (true) {
                                        System.out.println("Введіть IQ");
                                        String childIQ = sc.next();
                                        if (isInt(childIQ)) {
                                            cIQ = toInt(childIQ);
                                            if (validate(cIQ, 101)) {
                                                break;
                                            }
                                        }
                                    }
                                    try {
                                        controller.adoptChild(controller.getFamilyByIndex(ID), new Human(),
                                                childName, childSurname, birthDate, cIQ);
                                    }
                                    catch (FamilyOverflowException ex) {
                                        System.out.println(ex.getMessage());
                                    }
                                    System.out.println(mainMenu);
                                    break;
                                }
                                else if (q == 3)
                                    System.out.println(mainMenu);
                                    break;
                                }
                                else System.out.println("Виберіть доступну опцію");
                            }
                        else System.out.println("Введіть число");
                    }
                    }

                else if (x == 9) {
                    while (true) {
                        System.out.println("Введіть вік, з якого діти видяляються зі списку");
                        String str = sc.next();
                        if (isInt(str)) {
                            int q = toInt(str);
                            if (validate(q, 19)) {
                                controller.deleteAllChildrenOlderThen(q);
                                System.out.print(mainMenu);
                                break;
                            }
                            else System.out.println("Діти так довго не живуть");
                        }
                    }
                }
                else if (x == 10) {
                    controller.writter();
                    System.out.println("Дані збережено в файл");
                    System.out.println(mainMenu);
                }
            }
            else if (s.equalsIgnoreCase("exit")) {
                break;
            }
            else System.out.println("Виберіть доступну опцію");
        }
    }
}
