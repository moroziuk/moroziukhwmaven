package HomeWork3;

import java.io.InputStream;
import java.util.Scanner;

public class TaskPlanner {
    static String[][] schedule = new String[7][2];
    static void fill(String[][] data) {
        data [0][0] = "Sunday";
        data [0][1] = "Write code";
        data [1][0] = "Monday";
        data [1][1] = "Get some money";
        data [2][0] = "Tuesday";
        data [1][1] = "Kiss your wife";
        data [3][0] = "Wednesday";
        data [3][1] = "Write code";
        data [4][0] = "Thursday";
        data [4][1] = "Get some money";
        data [5][0] = "Friday";
        data [5][1] = "Write code";
        data [6][0] = "Saturday";
        data [6][1] = "Kiss your wife";
    }

     public static int validate(String x) {
         int count = 10;
         for (int i = 0; schedule.length> i; i++) {
             if (x.trim().equalsIgnoreCase(schedule[i][0])) count = i;
         }
         return count;
     }

    public static void main(String[] args) {
        fill(schedule);
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);
        System.out.println("Please, input the day of the week:");

       while (true) {
            String s = scanner.nextLine();
            int v = validate(s);

         if (v!=10) {
                System.out.printf(
                        "Your tasks for %s: %s.", schedule[v][0],schedule[v][1]);}
            else if ("exit".equals(s)) break;
            else System.out.println(
                    "Sorry, I don't understand you, please try again.");

            }

    }

}
