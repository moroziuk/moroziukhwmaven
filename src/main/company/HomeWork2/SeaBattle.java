package HomeWork2;

import java.io.InputStream;
import java.util.Scanner;

public class SeaBattle {

    public String represent(int value) {
        return switch (value) {
            case 1 -> "-";
            case 2 -> "X";
            case 3 -> "*";
            default -> "-";
        };
    }

    static boolean isInt(String raw) {
        try {
            int n = Integer.parseInt(raw);
            return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    static int toInt(String raw) {
        return Integer.parseInt(raw);
    }

    static int r = (int) (Math.random() * 3);
    static int r2 = (int) (Math.random() * 5);

    static boolean validateHigher(int x) {
        return x > 4;
    }

    static boolean validateLower(int x) {
        return x < 0;
    }

    public void arrangeShips(int[][] board) {
        int W = board[0].length;
        int H = board.length;
        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                board[y][x] = 0;
            }

            board[r][r2] = 1;
            board[r + 1][r2] = 1;
            board[r + 2][r2] = 1;
        }
    }

    public String representBoard(int[][] board) {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < board.length; y++) {
            sb.append("| ");
            for (int x = 0; x < board[y].length; x++) {
                if (x > 0) sb.append(" | ");
                sb.append(represent(board[y][x]));
            }
            sb.append(" |\n");
        }
        return sb.toString();
    }

    public void drawBoard(int[][] board) {
        System.out.println(representBoard(board));
    }

    public static void main(String[] args) {
        System.out.println("All Set. Get ready to rumble!");
        System.out.println("Please, insert the number");
        final int SIZE = 5;
        int[][] board = new int[SIZE][SIZE];
        SeaBattle game = new SeaBattle();
        InputStream in = System.in;
        Scanner sc = new Scanner(in);
        game.arrangeShips(board);
        int count = 0;

        while (count < 3) {
            String s = sc.nextLine();
            int x;
            int y;

            if (isInt(s)) {
                y = toInt(s);
                if (validateHigher(y))
                    System.out.println("Your number is too big. Please, try again.");
                else if (validateLower(y))
                    System.out.println("Your number is too small. Please, try again.");
                else
                    System.out.println("Please, insert the number");

                String g = sc.nextLine();
                if (isInt(g)&&(!(validateHigher(y))&&!(validateLower(y)))) {
                    x = toInt(g);
                    if (validateHigher(x))
                        System.out.println("Your number is too big. Please, try again.");
                    else if (validateLower(x))
                        System.out.println("Your number is too small. Please, try again.");
                    else if (board[x][y] == 1) {
                        board[x][y] = 2;
                        game.drawBoard(board);
                        count++;
                    } else if (board[x][y] == 0) {

                        board[x][y] = 3;
                        game.drawBoard(board);
                    } else if (board[x][y] == 2 || board[x][y] == 3) {
                        game.drawBoard(board);
                        System.out.println("You have already shot here. Please, insert the number");
                    }

                } else System.out.println("Please, insert the number");

            } else System.out.println("This is not a number. Please, insert the number");
        }
        System.out.println("You have won!");
    }
}
