package HomeWork1;

import java.io.InputStream;
import java.util.Scanner;

public class RandomApp {
    static int r = (int) (Math.random() * 101);

    static boolean isInt(String raw) {
        try {
            int n = Integer.parseInt(raw);
            return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    static int toInt(String raw) {
        return Integer.parseInt(raw);
    }

    static boolean validateHigher(int x) {
        return x > r;
    }
    static boolean validateLower(int x) {
        return x < r;
    }


    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner sc = new Scanner(in);

        System.out.print("Let the game begin!");
        String name;
        name = sc.next();
        while (true) {

            String s = sc.nextLine();
            int x;

            if (isInt(s)) {
                x = toInt(s);
                if (validateHigher(x)) System.out.println("Your number is too big. Please, try again.");
                else if (validateLower(x)) System.out.println("Your number is too small. Please, try again.");
                else System.out.printf("Congratulations, %s!", name);
            } else System.out.println("Please, insert the number");
        }

    }
}

